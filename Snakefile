from snakemake.utils import validate
import json
print(json.dumps(config, indent=4))
validate(config, "config.schema.json")
print(json.dumps(config, indent=4))


name = config["name"]

rule run_minimap:
        input:
                config['ref'],
                config['query']
        output:
                ref_sam = f"{name}.sam"
        conda: 
                "env.yml"
        shell:
                """
                minimap2 -t 12 -ax asm5 --eqx {input[0]} {input[1]} > {output[0]}
                """
